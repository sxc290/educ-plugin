(function() {
    'use strict';

    describe("MonthsController", function() {
        var today = new Date(),
            api,
            rootScope,
            scope,
            controller;

        beforeEach(function() {

            module('app');

            inject(function($controller, $rootScope, API) {
                api = API;
                rootScope = $rootScope;
                scope = $rootScope.$new();

                controller = $controller('MonthsController', {
                    $scope: scope,
                });
            });
        });

        it('Should be defined', function() {
            expect(controller).not.toBe(undefined);
        });

        it('Should be vars initalized', function() {
            expect(controller.year).toBe(today.getFullYear().toString().substr(2,2));
            expect(controller.currentYear).toBe(today.getFullYear());
        });

        it('Should be able to go to previous year', function() {

            spyOn(controller, 'previousYear').and.callThrough();
            controller.previousYear();

            expect(controller.currentYear).toBe(today.getFullYear() - 1);
        });

        it('Should be able to go to next year', function() {

            spyOn(controller, 'previousYear').and.callThrough();
            spyOn(controller, 'nextYear').and.callThrough();

            controller.previousYear();
            expect(controller.currentYear).toBe(today.getFullYear() - 1);

            controller.nextYear();
            expect(controller.currentYear).toBe(today.getFullYear());
        });

        it('Should be able to prevent next year be more than to current year', function() {

            spyOn(controller, 'nextYear').and.callThrough();
            controller.nextYear();

            expect(controller.nextYear).toHaveBeenCalled();
            expect(controller.currentYear).toBe(today.getFullYear());
        });

        it('Shoulb be able to broadcast a month changing', function() {

            spyOn(controller, 'setCurrentMonth').and.callThrough();
            spyOn(api, 'broadcast').and.callThrough();

            controller.setCurrentMonth('Jan');

            expect(controller.setCurrentMonth).toHaveBeenCalled();
            expect(controller.currentMonth).toBe('Jan');
            expect(api.broadcast).toHaveBeenCalled();
        });
    });

})();
