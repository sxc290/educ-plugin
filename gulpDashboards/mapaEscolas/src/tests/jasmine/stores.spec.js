(function() {
    'use strict';

    describe("StoreController", function() {
        var today = new Date(),

            api,
            dashboard,
            components,

            element,

            compile,
            rootScope,
            scope,
            controller,

            date = {
                year: today.getFullYear(),
                month: today.getMonth(),
                startDay: 1,
                endDay: today.getDate()
            };

        beforeEach(function() {

            module('app');

            inject(function(Dashboard, Components, API, $controller, $rootScope, $compile, $templateCache) {

                api = API;
                dashboard = Dashboard;
                components = Components;

                compile = $compile;
                rootScope = $rootScope;
                scope = $rootScope.$new();

                // var template = "<div ng-controller=\"StoreController as sc\"><div class=\"store-item col-xs-12\" ng-repeat=\"store in sc.stores\" ng-click=\"sc.toggleDetails($event, $index)\"><div class=\"store-title col-xs-12\"><span class=\"storebadge\">{{store.name}}</span> <span class=\"icon pull-right\"><i class=\"fa fa-chevron-down arrow-icon\"></i></span></div><div class=\"store-totalfield col-xs-12 col-sm-6 col-md-6 col-lg-3\"><span class=\"title\">{{store.total.title}}</span> <span class=\"value\"><span class=\"prefix\">{{store.total.prefix}}</span> {{store.total.value}}</span></div><div class=\"store-field col-xs-12 col-sm-6 col-md-6 col-lg-3\" ng-repeat=\"target in store.targets\"><div class=\"c100 p{{target.percent.class}} very-small\" ng-class=\"sc.getProgressColor(target.percent.value)\"><span>{{target.percent.value}}%</span><div class=\"slice\"><div class=\"bar\"></div><div class=\"fill\"></div></div></div><div class=\"target\"><span class=\"title\">{{target.title}}</span> <span class=\"value\"><span class=\"prefix\">{{target.prefix}}</span> {{target.value}}</span></div></div><div class=\"store-field col-xs-12 col-sm-6 col-md-6 col-lg-3\"><i class=\"icon fa fa-ticket\"></i><div class=\"target\"><span class=\"title\">{{store.ticket.title}}</span> <span class=\"value\"><span class=\"prefix\">{{store.ticket.prefix}}</span> {{store.ticket.value}}</span></div></div></div></div>";
                // element = compile(template)(scope);

                controller = $controller('StoreController', {
                    $scope: scope,
                    // $element: element
                });

                // scope.$digest();
            });
        });

        it('Should be defined', function() {
            expect(controller).not.toBe(undefined);
        });

        it('Should be able to initialize stores with current month', function(done) {

            var tri = ['T1', 'T1', 'T1', 'T2', 'T2', 'T2', 'T3', 'T3', 'T3', 'T4', 'T4', 'T4'],
                month = '[Data da Venda].[Ano da Venda].[Trimestre da Venda].[Mes da Venda]'
                            .replace('Ano da Venda', date.year)
                            .replace('Trimestre da Venda', tri[date.month])
                            .replace('Mes da Venda', date.month + 1),
                interval = month + '.[' + date.startDay + '] : ' + month + '.[' + date.endDay + ']';

            spyOn(controller, 'generateStoresComponent').and.callThrough();
            spyOn(controller, 'generateStores').and.callThrough();

            spyOn(dashboard, 'setParameters').and.callThrough();
            spyOn(dashboard, 'addComponent').and.callThrough();
            spyOn(dashboard, 'init').and.callThrough();

            spyOn(components, 'newComponent').and.callThrough();

            controller.generateStoresComponent(date);
            dashboard.setParameters([
                { param: 'interval', value: interval },
                { param: 'month',    value: month    }
            ]);
            var component = components.newComponent('QueryComponent', 'stores', {
                name: "stores",
                type: "queryComponent",
                parameters: [
                    ['interval', 'interval'],
                    ['month', 'month']
                ],
                listeners: ['interval', 'month'],
                valueAsId: false,
                queryDefinition: {
                    path: "/retail.cda",
                    dataAccessId: "stores"
                },
                // htmlObject:
                executeAtStart: true,
                priority: 5,
                postFetch: function(data) {

                    if(data) {
                        controller.generateStores(data);
                    }

                    expect(controller.generateStores).toHaveBeenCalled();
                    expect(controller.stores.length).toBeGreaterThan(0);

                    done();
                },
            });
            dashboard.addComponent(component);
            dashboard.init(component);


            expect(controller.generateStoresComponent).toHaveBeenCalled();
            expect(dashboard.setParameters).toHaveBeenCalled();
            expect(components.newComponent).toHaveBeenCalled();
            expect(dashboard.addComponent).toHaveBeenCalled();
            expect(dashboard.addComponent).toHaveBeenCalled();
        });

    });
})();
