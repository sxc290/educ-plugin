'use strict';

/**
 * @memberof app
 * @ngdoc directive
 * @name main
 * @description main directive
 * */
angular.module('app').directive('main', main);

function main() {
    var directive = {
        restrict: 'EA',
        templateUrl: 'app/pages/store/main.html',
    }

    return directive;
}

angular.module('app').directive('toggleClass', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                element.toggleClass(attrs.toggleClass);
            });
        }
    };
});

angular.module('app').directive('scatterPlot', function($window) {
    return {
        restrict: 'E',
        scope: {
            scatterdata: '='
        },
        link: function(scope, element, array) {

            var data = scope.scatterdata;
            var el = element[0];
            var w = element.context.offsetParent.clientWidth;
            var h = 200;
            var padding = 25;

            console.log("element ", element.context.offsetParent.clientWidth);


            //Create scale functions
            var xScale = d3.scaleLinear()
                .domain([0, d3.max(data, function(d) {
                    return 10;
                })])
                .range([padding, w - padding * 2]);

            var yScale = d3.scaleLinear()
                .domain([0, d3.max(data, function(d) {
                    return d[1];
                })])
                .range([h - padding, padding]);

            var rScale = d3.scaleLinear()
                .domain([0, d3.max(data, function(d) {
                    return d[1];
                })])
                .range([4, 14]);

            // Create axes
            var xAxis = d3.axisBottom()
                .scale(xScale)
                .ticks(5);

            var yAxis = d3.axisLeft()
                .scale(yScale)
                .ticks(5);

            //Create SVG element
            var svg = d3.select(el)
                .append("svg")
                .attr('id', 'scatter-plot')
                .attr("width", w)
                .attr("height", h)
                .attr('viewBox', '0 0 ' + element.context.offsetParent.clientWidth + ' 200')
                .attr('preserveAspectRatio', 'xMinYMin');


            svg.append("g") // create new g
                .attr('id', 'circles') // assign id of "circles"
                .attr('clip-path', 'url(#chart-area)') // add reference to clipPath
                .selectAll('circle')
                .data(data)
                .enter()
                .append("circle")
                .attr("cx", function(d) {
                    return xScale(d[0]);
                })
                .attr("cy", function(d) {
                    return yScale(d[1]);
                })
                .attr("r", function(d) {
                    return rScale(d[1]);
                });

            svg.append('g')
                .attr('class', 'axis')
                .attr('transform', 'translate(0,' + (h - padding) + ')') // sends axis to bottom
                .call(xAxis);

            svg.append('g')
                .attr('class', 'axis')
                .attr('transform', 'translate(' + padding + ',0)')
                .call(yAxis);

            svg.append('clipPath')
                .attr('id', 'chart-area')
                .append('rect')
                .attr('x', padding)
                .attr('y', padding)
                .attr('width', w - padding * 3)
                .attr('height', h - padding * 2);

            /* ------ Render Specs ----- */
            scope.render = function(newData, oldData) {
                var data = newData || data;

                svg.selectAll("circle")
                    .data(data)
                    .transition()
                    .duration(400) // milliseconds, so 1 second duration
                    .on('start', function() {
                        d3.select(this)
                            .attr('fill', '#EA5A5A')
                    })
                    .attr("cx", function(d) {
                        return xScale(d[0]);
                    })
                    .attr("cy", function(d) {
                        return yScale(d[1]);
                    })
                    .attr("r", function(d) {
                        return rScale(d[1]);
                    })
                    .on('end', function() {
                        d3.select(this)
                            .attr('fill', '#28BD8B')
                    });

                svg.append("g") // create new g
                    .attr('id', 'circles') // assign id of "circles"
                    .attr('clip-path', 'url(#chart-area)') // add reference to clipPath


                svg.append('g')
                    .attr('class', 'axis')
                    .attr('transform', 'translate(0,' + (h - padding) + ')') // sends axis to bottom
                    .call(xAxis);

                svg.append('g')
                    .attr('class', 'axis')
                    .attr('transform', 'translate(' + padding + ',0)')
                    .call(yAxis);

            };



            function updateWindow() {
                var scatter = d3.select('#scatter-plot');
                scatter.attr("width", '100%');
                scatter.attr("height", '100%');
            }

            angular.element($window).bind('resize', function() {
                updateWindow();
            })

            scope.$watch('scatterdata', function(newData, oldData) {
                console.log('watch fired');
                scope.render(newData, oldData);
            }, true);

        }
    };
});

angular.module('app').directive('myMap', function() {
    // directive link function
    var link = function(scope, element, attrs) {
        var map, infoWindow;
        var markers = [];

        // map config
        var mapOptions = {
            center: new google.maps.LatLng(50, 2),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };

        // init the map
        function initMap() {
            if (map === void 0) {
                map = new google.maps.Map(element[0], mapOptions);
            }
        }

        // place a marker
        function setMarker(map, position, title, content) {
            var marker;
            var markerOptions = {
                position: position,
                map: map,
                title: title,
                icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png'
            };

            marker = new google.maps.Marker(markerOptions);
            markers.push(marker); // add marker to array

            google.maps.event.addListener(marker, 'click', function() {
                // close window if not undefined
                if (infoWindow !== void 0) {
                    infoWindow.close();
                }
                // create new window
                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, marker);
            });
        }

        // show the map and place some markers
        initMap();

        setMarker(map, new google.maps.LatLng(51.508515, -0.125487), 'London', 'Just some content');
        setMarker(map, new google.maps.LatLng(52.370216, 4.895168), 'Amsterdam', 'More content');
        setMarker(map, new google.maps.LatLng(48.856614, 2.352222), 'Paris', 'Text here');
    };

    return {
        restrict: 'A',
        template: '<div id="map"></div>',
        replace: true,
        link: link
    };
});