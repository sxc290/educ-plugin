(function() {
    'use strict';

    angular.module('app')
        .controller('MainController', MainController);

    MainController.$inject = ['Dashboard', 'Components', '$scope'];

    function MainController(Dashboard, Components, $scope) {

        $scope.municipios = [
            { nome: "magé" },
            { nome: "rio das ostras" },
            { nome: "guapimirim" },
            { nome: "cachoeira de macacu" }
        ];

        $scope.dynamicPopover = {
            templateUrlMunicipio: 'myPopoverTemplate.html',
            templateUrlDetalhamento: 'popoverDetalhamento.html'
        };

        $scope.footerDetalhamento = "";

        $scope.funFooterDetalhamento = function() {
            console.log("funFooterDetalhamento", $scope.footerDetalhamento)
            if ($scope.footerDetalhamento == "footerDetalhamento") {
                $scope.footerDetalhamento = false;
            } else {
                $scope.footerDetalhamento = "footerDetalhamento";
            }
        }


        function teste() {
            console.log("teste teste");
        }

        function olhaMes(tempo) {
            if (tempo == 1) {
                return "Janeiro";
            } else if (tempo == 2) {
                return "Fevereiro";
            } else if (tempo == 3) {
                return "Mar&ccedil;o";
            } else if (tempo == 4) {
                return "Abril";
            } else if (tempo == 5) {
                return "Maio";
            } else if (tempo == 6) {
                return "Junho";
            } else if (tempo == 7) {
                return "Julho";
            } else if (tempo == 8) {
                return "Agosto";
            } else if (tempo == 9) {
                return "Setembro";
            } else if (tempo == 10) {
                return "Outubro";
            } else if (tempo == 11) {
                return "Novembro";
            } else if (tempo == 12) {
                return "Dezembro";
            }
        }

        
        function criaMapa() {

            var componentMapa = Components.newComponent('NewMapComponent', 'mapa', {
                type: "NewMapComponent",
                name: "mapa",
                priority: 5,
                executeAtStart: true,
                htmlObject: "map",
                parameters: [],
                listeners: [],
                preExecution: function preexecution() {},
                postFetch: function postfetch(dados) {},
                centerLongitude: $scope.longitudeClientes,
                centerLatitude: $scope.latitudeClientes,
                defaultZoomLevel: 11,
                maxZoom: 25,
                mapEngineType: "openlayers",
                API_KEY: "AIzaSyAuTfjeCYgxZ-N8r9SbXiNsE6QU2YccLis",
                marker: "https://s3-sa-east-1.amazonaws.com/a4pmstatic/img/circ-verde.png",
                markerWidth: 30,
                markerHeight: 30,
                popupParameters: ["teste "],
                cggGraphParameters: [],
                markerClickFunction: function(d) {
                    console.log("clicou no mapa", d);

                    console.log("id ", $('#' + d.feature.geometry.id));

                    document.getElementById(d.feature.geometry.id).setAttributeNS("http://www.w3.org/2000/svg", "xlink:href", "https://s3-sa-east-1.amazonaws.com/a4pmstatic/img/circ-verm.png")
                    

                    var data = new Date(d.data[6]);
                    console.log(data.getDay() + "/" + data.getMonth() + "/" + data.getFullYear());

                    var valueData = olhaMes((data.getMonth() + 1 )) + "/" + data.getFullYear();

                    $scope.infoEscola = {
                        nomeEscola: d.data[3],
                        diretor: d.data[4],
                        telefone: d.data[5],
                        dataFundacao: valueData,
                        endereco: d.data[7],
                        cidade: d.data[8],
                        siglaEstado: d.data[9],
                        qtd_turmas: d.data[10],
                        qtd_alunos: d.data[11],
                        meninos: d.data[12],
                        meninas: d.data[13]
                    }

                    $(".titulo-popover").text(d.data[3]);
                    $("#divTelefone").text(d.data[5]);
                    $("#divFundacao").text(valueData);
                    $("#divEndereço").text(d.data[7]);


                    $('#featurePopup_close').removeClass("olPopupCloseBox");
                    //$('#featurePopup_close').removeClass("glyphicon glyphicon-remove");olPopupCloseBox

                    console.log("infoEscola", $scope.infoEscola);
                    $scope.$digest();

                    $scope.$apply();

                },
                popupWidth: 300,
                popupHeight: 180,
                popupContentsDiv: "popoverDetalhamento",
                mapMode: "markers",
                locationResolver: "mapquest",
                colormap: [],
                tilesets: "google",
                queryDefinition: {
                    dataAccessId: "sql_mapa_escolas",
                    path: "/mapaEscolas.cda"
                }
            });

            Dashboard.addComponent(componentMapa);
            Dashboard.init(componentMapa);

        }

        criaMapa();

        $scope.alert = window.alert;

    }

})();