'use strict';

/**
 * @memberof app
 * @ngdoc directive
 * @name loader
 * @description Loader directive
 * */
angular.module('app').directive('loader', loader);

function loader() {
    var directive = {
        restrict: 'E',
        template: '<div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>'
    }

    return directive;
}
