(function() {
    'use strict';

    angular.module('app', ['ngResource', 'ui.router', 'ui.bootstrap']); // , 'uiGmapgoogle-maps']);

    angular.module('app').config(configuration);

    configuration.$inject = ['$compileProvider']; // , 'uiGmapGoogleMapApiProvider'];

    function configuration($compileProvider) { //, uiGmapGoogleMapApiProvider) {
        // remove ng-scope used to debug
        $compileProvider.debugInfoEnabled(false);
        /*uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyD-VKKpbD09K2ZxfDMCXNUvgKaig96hoIg',
            v: '3.26', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });*/
    }

})();