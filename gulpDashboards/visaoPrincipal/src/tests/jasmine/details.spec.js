(function() {
    'use strict';

    describe("StoreDetailsController", function() {
        var today = new Date(),

            api,
            dashboard,
            components,

            rootScope,
            scope,
            controller,

            _interval = '[Data da Venda].[2016].[T2].[6].[1] : [Data da Venda].[2016].[T2].[6].[30]',
            _date = {
                year: today.getFullYear(),
                month: today.getMonth(),
                startDay: 1,
                endDay: today.getDate()
            };

        beforeEach(function() {

            module('app');

            inject(function(Dashboard, Components, API, $controller, $rootScope) {

                api = API;
                dashboard = Dashboard;
                components = Components;

                rootScope = $rootScope;
                scope = $rootScope.$new();

                controller = $controller('StoreDetailsController', {
                    $scope: scope,
                });
            });
        });

        it('Should be defined', function() {
            expect(controller).not.toBe(undefined);
        });

        it('Should be able to load details', function() {

            // var storeData = {
            //     date: _date,
            //     interval: _interval,
            //     store: 'MATRIZ'
            // }
            //
            // scope.$apply(function() {
            //    rootScope.$broadcast('store', storeData);
            // });
            //
            // expect(scope.success).toBe(true);

            spyOn(api, 'broadcast').and.callThrough();
            spyOn(rootScope, '$broadcast').and.callThrough();
            // spyOn(scope, '$on').and.callThrough();
            spyOn(controller, 'generateStoreDetailsComponent').and.callThrough();

            var storeData = {
                date: _date,
                interval: _interval,
                store: 'MATRIZ'
            }

            api.broadcast('store', storeData);
            // controller.generateStoreDetailsComponent(storeData);

            expect(api.broadcast).toHaveBeenCalled();
            expect(rootScope.$broadcast).toHaveBeenCalledWith('store', storeData);
            // expect(controller.storeData).not.toBe(undefined);
            // expect(scope.$on).toHaveBeenCalled();
            // expect(controller.generateStoreDetailsComponent).toHaveBeenCalled();
        });

    });
})();
