(function() {
    'use strict';

    /**
     * @memberof app
     * @ngdoc service
     * @name Helpers
     * @description Helpers service to help functions
     * */
    angular.module('app').factory("Helpers", HelpersService);

    HelpersService.$inject = [];

    function HelpersService() {

        var _browser = {
            // Opera 8.0+
            isOpera: (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,
            // Firefox 1.0+
            isFirefox: typeof InstallTrigger !== 'undefined',
            // At least Safari 3+: "[object HTMLElementConstructor]"
            isSafari: Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0,
            // Internet Explorer 6-11
            isIE: /*@cc_on!@*/false || !!document.documentMode,
            // Edge 20+
            isEdge: !this.isIE && !!window.StyleMedia,
            // Chrome 1+
            isChrome: !!window.chrome && !!window.chrome.web
        }

        var _monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            _monthNamesInitials = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];

        var service = {
            browser: _browser,
            getMonthNames: getMonthNames,
            getMonthIndexByName: getMonthIndexByName,
            getMonthInitials: getMonthInitials,
            getMonthInitialsByIndex: getMonthInitialsByIndex,
            formatBrowserDate: formatBrowserDate,
            getFormattedDate: getFormattedDate,
            getSpanFormattedDate: getSpanFormattedDate,
            getFormattedTime: getFormattedTime,
            getFormattedCurrency: getFormattedCurrency
        }

        return service;

        ////////////

        /**
         * @memberof Helpers
         * @function getMonthNames
         * @description get month names
         * @returns {Array} array of month names
         */
        function getMonthNames() {
            return _monthNames;
        }

        /**
         * @memberof Helpers
         * @function getMonthIndexByName
         * @description get month index by name
         * @param {String} name - month name
         * @returns {Number} month index
         */
        function getMonthIndexByName(name) {
            return _monthNames.indexOf(name);
        }

        /**
         * @memberof Helpers
         * @function getMonthInitials
         * @description get month initials
         * @returns {Array} array of month initials
         */
        function getMonthInitials() {
            return _monthNamesInitials;
        }

        /**
         * @memberof Helpers
         * @function getMonthInitialsByIndex
         * @description get month initials by index
         * @returns {String} month initials
         */
        function getMonthInitialsByIndex(index) {
            return _monthNamesInitials[index];
        }

        /**
         * @memberof Helpers
         * @function formatBrowserDate
         * @description format date to brazil pattend in brownsers
         * @param  {String} date - data in a string format
         * @returns {String} date in a brazilian string format (dd/mm/yyyy)
         */
        function formatBrowserDate(date) {

            // fix date in safari and firefox
            if((_browser.isSafari || _browser.isFirefox) && date) {
                var dt = date.split('/');
                date = dt[1] + "/" + dt[0] + "/" + dt[2];
            }

            return date;
        }

        /**
         * @memberof Helpers
         * @function getFormattedDate
         * @description get date in dd/mm/yyyy format
         * @param  {String|Date} date - date
         * @returns {String} date in a brazilian string format (dd/mm/yyyy)
         */
        function getFormattedDate(dt) {
            var date = new Date(dt),
                day, month, year;

            if(date) {
                day = date.getDate();
                month = date.getMonth() + 1;
                year = date.getFullYear();
            }

            if(date && (_browser.isFirefox || _browser.isSafari)) {

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                return day + "/" + month + "/" + year;
            }

            else if(date) {
                var day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                return year + "-" + month + "-" + day;
            }
        }

        /**
         * @memberof Helpers
         * @function getSpanFormattedDate
         * @description get date in yyyy format
         * @param  {String|Date} date - data
         * @returns {String} date in a brazilian string format (dd-mm-yyyy)
         */
        function getSpanFormattedDate(dt) {
            var date = new Date(dt);

            if(date) {
                var day = date.getDate(),
                    month = date.getMonth() + 1,
                    year = date.getFullYear();

                if (day.toString().length == 1) {
                    day = "0" + day;
                }

                if (month.toString().length == 1) {
                    month = "0" + month;
                }

                return day + "/" + month + "/" + year;
            }
        }

        /**
         * @memberof Helpers
         * @function getFormattedTime
         * @description get time in hh:mm:ss format
         * @param  {String|Date} date - data
         * @returns {String} date in format (hh:mm:ss)
         */
        function getFormattedTime(dt) {
            var date = new Date(dt);

            if(date) {
                var seconds = date.getSeconds(),
                    minutes = date.getMinutes(),
                    hours = date.getHours();

                if (seconds.toString().length == 1) {
                    seconds = "0" + seconds;
                }

                if (minutes.toString().length == 1) {
                    minutes = "0" + minutes;
                }

                if (hours.toString().length == 1) {
                    hours = "0" + hours;
                }

                return hours + ":" + minutes + ":" + seconds;
            }
        }

        function getFormattedCurrency(data) {
            return data.toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }

    }
})();
