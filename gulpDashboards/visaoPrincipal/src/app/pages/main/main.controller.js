(function() {
    'use strict';

    angular.module('app').controller('MainController', MainController);

    MainController.$inject = ['Dashboard', 'Components', '$scope'];

    function MainController(Dashboard, Components, $scope) {


        $scope.cards = [{
                municipio: "cachoeira de macacu",
                notaObtida: "3,1",
                meta: "4,2",
                notas: {
                    boa: 28.5,
                    media: 21.5,
                    ruim: 33.3,
                    pessima: 15.7
                }
            },
            {
                municipio: "guapimirim",
                notaObtida: "2,6",
                meta: "3,9",
                notas: {
                    boa: 28.5,
                    media: 21.5,
                    ruim: 33.3,
                    pessima: 15.7
                }
            },
            {
                municipio: "rio das ostras",
                notaObtida: "3,9",
                meta: "4,7",
                notas: {
                    boa: 28.5,
                    media: 21.5,
                    ruim: 33.3,
                    pessima: 15.7
                }
            },
            {
                municipio: "magé",
                notaObtida: "2,8",
                meta: "3,6",
                notas: {
                    boa: 28.5,
                    media: 21.5,
                    ruim: 33.3,
                    pessima: 15.7
                }
            }
        ];



    }

})();