'use strict';

/**
 * @memberof app
 * @ngdoc directive
 * @name main
 * @description main directive
 * */
angular.module('app').directive('main', main);

function main() {
    var directive = {
        restrict: 'EA',
        templateUrl: 'app/pages/store/main.html',
    }

    return directive;
}

angular.module('app').directive('toggleClass', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                element.toggleClass(attrs.toggleClass);
            });
        }
    };
});