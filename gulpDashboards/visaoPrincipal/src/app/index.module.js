(function() {
    'use strict';

    angular.module('app', ['ngResource', 'ui.router', 'ui.bootstrap', 'angular-owl-carousel']);

    angular.module('app').config(configuration);

    configuration.$inject = ['$compileProvider'];

    function configuration($compileProvider) {
        // remove ng-scope used to debug
        $compileProvider.debugInfoEnabled(false);
    }

})();
