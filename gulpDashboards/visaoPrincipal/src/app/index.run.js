(function() {
    'use strict';

    // set title and viewport
    $('title').text('Educ');
    $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">');
    //$('head').append('<script language="javascript" type="text/javascript" src="/pentaho/static/js/ga-a4pmdw.js"></script>');

    var dashboard, components;

    require(['cdf/Dashboard.Bootstrap', 'cdf/components/QueryComponent', 'cde/components/NewMapComponent'],
        function(Dashboard, QueryComponent, NewMapComponent) {

            dashboard = Dashboard;
            components = {
                'QueryComponent': QueryComponent,
                'NewMapComponent': NewMapComponent
            }

            bootstrap();
        });

    // bootstrap();

    angular.module('app').run(runBlock);

    /** @ngInject */
    function runBlock(Dashboard, Components, $log) {

        Dashboard.setDashboard(new dashboard());
        Components.setComponentsClasses(components);

        $log.debug('runBlock end');
    }

    function bootstrap() {
        angular.element(document).ready(function() {
            angular.bootstrap(document, ["app"]);
        });
    }

})();