var conf = require('./conf'),
    gulp = require('gulp'),
    shell = require('gulp-shell'),
    argv = require('yargs').argv,
    gulpZip = require('gulp-zip');

var pentaho_import = 'c:\\biserver-ce\\import-export.bat --import --url=http://localhost/pentaho --username=admin --password=A4PM@gestao123 --overwrite=true --permission=true --retainOwnership=true';
if (argv.e === 'hom') {
    var pentaho_import = 'c:\\biserver-ce\\import-export.bat --import --url=http://hpbis001.a4pm.com.br/pentaho --username=admin --password=A4PM@gestao123 --overwrite=true --permission=true --retainOwnership=true';
} else if (argv.e === 'prod') {
    var pentaho_import = 'c:\\biserver-ce\\import-export.bat --import --url=http://pbis001.a4pm.com.br/pentaho --username=admin --password=A4PM@gestao123 --overwrite=true --permission=true --retainOwnership=true';
}

console.log(pentaho_import);

var project_path = conf.paths.dist,

    src_path = "src", // user source path
    pentaho_dist_path = '/', // dist folder
    pentaho_path = '/' + project_path, // pentaho path
    zipfile_path = 'c:\\repositorio\\educ-plugin\\gulpDashboards\\visaoPrincipal\\zip', // user file path
    zip_path = 'zip', // user zip path

    // pentaho paths
    path = {
        bower: pentaho_path, // bower_components
        src: pentaho_path, // scripts/html
        styles: pentaho_path + '/' + src_path + '/app', // styles
        html: pentaho_path + '/' + src_path, // html (new files)
        cda: pentaho_path + '/cdas', // cda files
        dist: pentaho_dist_path, // dist

        testHtml: pentaho_path + '/' + src_path, // html (new files)
        test: pentaho_path, // scripts/html
    }

// zip file paths
file_path = {
        bower: zipfile_path + '\\bower_components.zip', // bower_components
        src: zipfile_path + '\\src.zip', // scripts/html
        styles: zipfile_path + '\\css.zip', // styles
        html: zipfile_path + '\\index.zip', // html (new files)
        cda: zipfile_path + '\\cda.zip', // cda files
        dist: zipfile_path + '\\dist.zip', // dist
        testHtml: zipfile_path + '/test.zip', // html (new files)
        test: zipfile_path + '/specs.zip', // specs
    },

    // zip commands
    zip = {
        bower: 'zip -r ' + zip_path + '/bower_components.zip ' + src_path + '/bower_components',
        src: 'zip -r ' + zip_path + '/src.zip ' + src_path + '/app -x "*.sass*" -x "*.DS_Store"',
        styles: 'zip -r ' + zip_path + '/css.zip -j .tmp/serve/app/index.css',
        html: 'zip -r ' + zip_path + '/index.zip -j .tmp/serve/ -x "*.css"',
        cda: 'zip -r ' + zip_path + '/cda.zip -j ' + src_path + '/assets/cdas -x "*.DS_Store"',
        dist: 'zip -r ' + zip_path + '/dist.zip ' + project_path,
        testHtml: 'zip -r ' + zip_path + '/test.zip -j .tmp/serve/test.html .tmp/serve/test.xcdf',
        test: 'zip -r ' + zip_path + '/specs.zip ' + src_path + '/tests -x "*.DS_Store"',
    };

/* SOURCE TASKS
=================================== */

gulp.task('zip:bower', function() {
    return gulp.src('./' + src_path + '/bower_components/**', {
            base: './'
        })
        .pipe(gulpZip('bower_components.zip'))
        .pipe(gulp.dest('./zip'));
});
gulp.task('import:bower', ['zip:bower'], shell.task([
    pentaho_import + ' --path=' + path.bower + ' --file-path=' + file_path.bower
]));

// *.js and *.html
gulp.task('zip:src', function() {
    return gulp.src('./' + src_path + '/app/**', {
            base: './'
        })
        .pipe(gulpZip('src.zip'))
        .pipe(gulp.dest('./zip'));
});
gulp.task('import-src', ['zip:src'], shell.task([
    pentaho_import + ' --path=' + path.src + ' --file-path=' + file_path.src
]));

// *.scss -> index.css
gulp.task('zip:src-style', function() {
    return gulp.src('./.tmp/serve/app/**')
        .pipe(gulpZip('css.zip'))
        .pipe(gulp.dest('./zip'));
});
gulp.task('import-src-styles', ['zip:src-style'], shell.task([
    pentaho_import + ' --path=' + path.styles + ' --file-path=' + file_path.styles
]));

// index.html
gulp.task('zip:src-index', function() {
    return gulp.src(['./.tmp/serve/**', '!./.tmp/serve/**/.css'])
        .pipe(gulpZip('index.zip'))
        .pipe(gulp.dest('./zip'));
});
gulp.task('import-src-html', ['zip:src-index'], shell.task([
    pentaho_import + ' --path=' + path.html + ' --file-path=' + file_path.html
]));

// *.cda
gulp.task('zip:cda', function() {
    return gulp.src('./' + src_path + '/assets/cdas/**', {
            base: './' + src_path + '/assets/cdas'
        })
        .pipe(gulpZip('cda.zip'))
        .pipe(gulp.dest('./zip'));
});
gulp.task('import-cda', ['zip:cda'], shell.task([
    pentaho_import + ' --path=' + path.cda + ' --file-path=' + file_path.cda
]));

/* TESTS TASKS
=================================== */

// index.html
gulp.task('import-test-html', shell.task([
    zip.testHtml,
    pentaho_import + ' --path=' + path.testHtml + ' --file-path=' + file_path.testHtml
]));

// *.spec.js
gulp.task('import-test', shell.task([
    zip.test,
    pentaho_import + ' --path=' + path.test + ' --file-path=' + file_path.test
]));

/* DIST TASKS
=================================== */
gulp.task('import:dist', ['clean'], function() {
    gulp.start('import-cda');
    gulp.start('build-dist');
});

gulp.task('build-dist', ['build'], function() {
    gulp.start('import-dist');
});

gulp.task('zip:dist', function() {
    return gulp.src(['./' + project_path + '/**', '!./' + project_path + '{/maps,/maps/**}'], {
            base: './'
        })
        .pipe(gulpZip('dist.zip'))
        .pipe(gulp.dest('./zip'));
});

gulp.task('import-dist', ['zip:dist'], shell.task([
    pentaho_import + ' --path=' + path.dist + ' --file-path=' + file_path.dist
]));